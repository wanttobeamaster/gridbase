module gitee.com/wanttobeamaster/gridbase

go 1.13

require (
	gitee.com/wanttobeamaster/bbolt v1.3.3
	gitee.com/wanttobeamaster/etcd v3.3.17+incompatible
	gitee.com/wanttobeamaster/go-nemo v0.0.0-20190419084252-5ec7afa882c6
	gitee.com/wanttobeamaster/go-semver v0.3.0 // indirect
	gitee.com/wanttobeamaster/pkg v0.0.0-20210415044500-3b8f7c31f95a
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/fagongzi/goetty v1.3.1
	github.com/fagongzi/log v0.0.0-20190424080438-6b79fa3fda5a
	github.com/fagongzi/util v0.0.0-20191031020235-c0f29a56724d
	github.com/garyburd/redigo v1.6.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	github.com/google/btree v1.0.1
	github.com/gorilla/mux v1.7.3
	github.com/montanaflynn/stats v0.5.0
	github.com/pilosa/pilosa v1.4.0
	github.com/pingcap/check v0.0.0-20190102082844-67f458068fc8
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.10.0
	github.com/prometheus/common v0.18.0
	github.com/shirou/gopsutil v2.19.9+incompatible
	github.com/unrolled/render v1.0.1
	github.com/urfave/negroni v1.0.0
	golang.org/x/net v0.0.0-20210414194228-064579744ee0
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba
	google.golang.org/grpc v1.37.0
)

replace github.com/gogo/protobuf v1.3.1 => github.com/gogo/protobuf v1.3.1

replace github.com/golang/protobuf v1.5.2 => github.com/golang/protobuf v1.3.2

replace github.com/google/btree v1.0.1 => github.com/google/btree v1.0.0

replace google.golang.org/grpc v1.37.0 => google.golang.org/grpc v1.24.0

replace gitee.com/wanttobeamaster/bbolt => ../bbolt

replace gitee.com/wanttobeamaster/go-systemd => ../go-systemd

replace gitee.com/wanttobeamaster/go-semver => ../go-semver

replace gitee.com/wanttobeamaster/pkg => ../pkg

replace gitee.com/wanttobeamaster/etcd => ../etcd

replace gitee.com/wanttobeamaster/c-nemo => ../c-nemo

replace gitee.com/wanttobeamaster/go-nemo => ../go-nemo
