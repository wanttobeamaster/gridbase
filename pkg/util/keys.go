// Copyright 2016 DeepFabric, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// See the License for the specific language governing permissions and
// limitations under the License.

package util

import (
	"encoding/binary"
	"hash/crc64"

	"gitee.com/wanttobeamaster/gridbase/pkg/pb/metapb"
	"github.com/fagongzi/goetty"
)

var (
	tab = crc64.MakeTable(crc64.ECMA)
	mp  = goetty.NewSyncPool(8, 16, 2)
)

// NoConvert no converter
func NoConvert(key []byte, do func([]byte) metapb.Cell) metapb.Cell {
	return do(key)
}

// Uint64Convert returns the hash crc64 result value, must use `ReleaseConvertBytes` to release
func Uint64Convert(key []byte, do func([]byte) metapb.Cell) metapb.Cell {
	b := mp.Alloc(8)
	binary.BigEndian.PutUint64(b, crc64.Checksum(key, tab))
	value := do(b)
	mp.Free(b)
	return value
}


type Trie struct {
	Next [128]*Trie
	Had bool
}

func NewTrie()*Trie{
	return &Trie{
		Next: [128]*Trie{},
		Had: false,
	}
}
// xiaoxiao : add dict Tree
func (this *Trie)BuildTrie(member []byte) bool {
	root := this
	for i := 0; i < len(member); i++ {
		idx := int(member[i])
		if root.Next[idx] == nil {
			root.Next[idx] = NewTrie()
		}
		root = root.Next[idx]
		if i == len(member) - 1 {
			if root.Had {
				return true
			}else{
				root.Had = true
				return false
			}
		}
	}
	return false
}