package storage

import "gitee.com/wanttobeamaster/gridbase/pkg/util"

func RawKeyPrefix(key []byte) []byte {
	buf := make([]byte, 4 + len(key))
	idx := 0
	util.Uint32ToBytes1(buf[idx:], uint32(len(key)))
	idx += 4
	copy(buf[idx:], key)
	return buf
}

//KVPrefixNext use Greedy algorithm, calculate next KVPrefix
func KVPrefixNext(key []byte) []byte {
	buf := make([]byte, len([]byte(key)))
	copy(buf, []byte(key))
	var i int
	for i = len(key) - 1; i >= 0; i-- {
		buf[i]++
		if buf[i] != 0 {
			break
		}
	}
	if i == -1 {
		copy(buf, key)
		buf = append(buf, 0)
	}
	return buf
}